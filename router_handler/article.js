// 岗位的处理函数模块
const path = require('path')
const db = require('../db/index')

// 发布岗位的处理函数
exports.addArticle = (req, res) => {
  console.log(req.file)
  if (!req.file || req.file.fieldname !== 'cover_img') return res.cc('文章封面是必选参数！')

  // TODO：证明数据都是合法的，可以进行后续业务逻辑的处理
  // 处理岗位的信息对象
  const articleInfo = {
    // 标题、内容、发布状态、所属分类的Id
    ...req.body,
    // 岗位封面的存放路径
    cover_img: path.join('/uploads', req.file.filename),
    // 岗位的发布时间
    pub_date: new Date(),
    // 岗位作者的Id
    author_id: req.user.id,
  }

  const sql = `insert into ev_articles set ?`
  db.query(sql, articleInfo, (err, results) => {
    if (err) return res.cc(err)
    if (results.affectedRows !== 1) return res.cc('发布新岗位失败！')
    res.cc('发布岗位成功！', 0)
  })
}

//岗位列表显示处理函数
exports.getArticle = (req, res) => {
  // const sql = 'select * from ev_articles where is_delete=0 order by id asc'
  const sql = `select aleft.Id as 'Id',
    aleft.title as 'title',
    aleft.content as 'content',
    aleft.pub_date as 'pub_date',
    aleft.state as 'state',
    aleft.is_delete as 'is_delete',
    aright.name as 'cate_name'
    from ev_articles as aleft left join ev_article_cate as aright on aleft.cate_id=aright.id
    where aleft.is_delete=0 order by aleft.Id asc`
  db.query(sql, (err, results) => {
    if (err) return res.cc(err)
    res.send({
      status: 0,
      message: '获取岗位列表成功！',
      data: results,
    })
  })
}

// 删除岗位分类的处理函数
exports.deleteById = (req, res) => {
  const sql = `update ev_articles set is_delete=1 where Id=?`

  db.query(sql, req.params.Id, (err, results) => {
    // 执行 SQL 语句失败
    if (err) return res.cc(err)

    // SQL 语句执行成功，但是影响行数不等于 1
    if (results.affectedRows !== 1) return res.cc('删除岗位失败！')

    // 删除岗位分类成功
    res.cc('删除岗位成功！', 0)
  })
}

//根据Id查询岗位的处理函数
exports.getArticleById = (req, res) => {
  const sql = `select * from ev_articles where Id=?`
  db.query(sql, req.params.Id, (err, results) => {
    // 执行 SQL 语句失败
    if (err) return res.cc(err)
    // SQL 语句执行成功，但是没有查询到任何数据
    if (results.length !== 1) return res.cc('获取岗位数据失败！')
    // 把数据响应给客户端
    res.send({
      success: true,
      status: 0,
      message: '获取岗位数据成功！',
      data: results[0],
    })
  })
}

//更新岗位的处理函数
exports.editArticle = (req, res) => {
  //先查询有没有这个岗位有没有名称撞车
  const sqlStr = `select * from ev_articles where Id != ? and title = ?`
  //执行查重操作
  db.query(sqlStr, [req.body.Id, req.body.title], (err, results) => {
    if (err) return res.cc(err)
    if (results[0]) {
      return res.cc('岗位标题不能重复！')
    }
    if (!req.file || req.file.fieldname !== 'cover_img')
      return res.cc('岗位封面是必选参数！')
    // 证明数据都是合法的，可以进行后续业务逻辑的处理
    // 处理岗位的信息对象
    const articleInfo = {
      // 标题、内容、发布状态、所属分类的Id
      ...req.body,
      // 岗位封面的存放路径
      cover_img: path.join('/uploads', req.file.filename),
      // 岗位的发布时间
      pub_date: new Date(),
      // 岗位作者的Id
      author_id: req.auth.id,
    }
    const sql = `update ev_articles set ? where Id=?`
    db.query(sql, [articleInfo, req.body.Id], (err, results) => {
      if (err) return res.cc(err)
      if (results.affectedRows !== 1) return res.cc('编辑岗位失败！')
      res.cc('编辑岗位成功！', 0)
    })
  })
}

